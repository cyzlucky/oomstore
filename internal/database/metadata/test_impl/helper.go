package test_impl

func boolPtr(i bool) *bool {
	return &i
}

func intPtr(i int) *int {
	return &i
}

func stringPtr(s string) *string {
	return &s
}
