package test_impl

import (
	"context"
	"math/rand"
	"testing"
	"time"

	"github.com/oom-ai/oomstore/pkg/oomstore/types"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/oom-ai/oomstore/internal/database/dbutil"
	"github.com/oom-ai/oomstore/internal/database/online"
)

type PrepareStoreFn func(*testing.T) (context.Context, online.Store)

type DestroyStoreFn func()

type Sample struct {
	Features types.FeatureList
	Revision *types.Revision
	Entity   *types.Entity
	Data     []types.ExportRecord
}

var SampleSmall Sample
var SampleMedium Sample

func init() {
	rand.Seed(time.Now().UnixNano())

	{
		SampleSmall = Sample{
			Features: types.FeatureList{
				&types.Feature{
					ID:        1,
					Name:      "age",
					FullName:  "user.age",
					GroupID:   1,
					Group:     &types.Group{ID: 1, Category: types.CategoryBatch},
					ValueType: types.Int64,
				},
				&types.Feature{
					ID:        2,
					Name:      "gender",
					FullName:  "user.gender",
					GroupID:   1,
					Group:     &types.Group{ID: 1, Category: types.CategoryBatch},
					ValueType: types.String,
				},
				&types.Feature{
					ID:        3,
					Name:      "account",
					FullName:  "user.account",
					GroupID:   1,
					Group:     &types.Group{ID: 1, Category: types.CategoryBatch},
					ValueType: types.Float64,
				},
				&types.Feature{
					ID:        4,
					Name:      "is_active",
					FullName:  "user.is_active",
					GroupID:   1,
					Group:     &types.Group{ID: 1, Category: types.CategoryBatch},
					ValueType: types.Bool,
				},
				&types.Feature{
					ID:        5,
					Name:      "register_time",
					FullName:  "user.register_time",
					GroupID:   1,
					Group:     &types.Group{ID: 1, Category: types.CategoryBatch},
					ValueType: types.Time,
				},
			},
			Revision: &types.Revision{
				ID:      3,
				GroupID: 1,
				Group: &types.Group{
					ID:       1,
					Category: types.CategoryBatch,
				},
			},
			Entity: &types.Entity{ID: 5, Name: "user"},
			Data: []types.ExportRecord{
				[]interface{}{"3215", int64(18), "F", 1.1, true, time.Now()},
				[]interface{}{"3216", int64(29), nil, 2.0, false, time.Now()},
				[]interface{}{"3217", int64(44), "M", 3.1, true, time.Now()},
			},
		}

	}

	{
		features := types.FeatureList{
			&types.Feature{
				ID:        2,
				Name:      "charge",
				FullName:  "user.charge",
				GroupID:   2,
				Group:     &types.Group{ID: 2, Category: types.CategoryBatch},
				ValueType: types.Float64,
			},
		}

		revision := &types.Revision{ID: 9, GroupID: 2, Group: &types.Group{ID: 2, Category: types.CategoryBatch}}
		entity := &types.Entity{ID: 5, Name: "user"}
		var data []types.ExportRecord

		for i := 0; i < 100; i++ {
			record := []interface{}{dbutil.RandString(10), rand.Float64()}
			data = append(data, record)
		}
		SampleMedium = Sample{features, revision, entity, data}
	}
}

func importSample(t *testing.T, ctx context.Context, store online.Store, samples ...*Sample) {
	for _, sample := range samples {
		stream := make(chan types.ExportRecord)
		go func(sample *Sample) {
			defer close(stream)
			for i := range sample.Data {
				stream <- sample.Data[i]
			}
		}(sample)

		err := store.Import(ctx, online.ImportOpt{
			Features:     sample.Features,
			Revision:     sample.Revision,
			Entity:       sample.Entity,
			ExportStream: stream,
		})
		require.NoError(t, err)
	}
}

func compareFeatureValue(t *testing.T, expected, actual interface{}, valueType types.ValueType) {
	if valueType == types.Time {
		expected, ok := expected.(time.Time)
		require.Equal(t, true, ok)

		actual, ok := actual.(time.Time)
		require.Equal(t, true, ok)

		if expected.Location() == actual.Location() {
			assert.Equal(t, expected.Local().Unix(), actual.Local().Unix())
		}
	} else {
		assert.Equal(t, expected, actual)
	}
}
